import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 默认用户头像
    userPhoto: 'https://img1.baidu.com/it/u=592570905,1313515675&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1663952400&t=44b289bf26131ad3a5510b9a9636598c',
    userName: '小主~'
  },
  getters: {
  },
  mutations: {
    // 编码风格，mutations里面最好大写，好对页面方法进行区分
    SET_USERPHOTO (state, val) {
      state.userPhoto = val
    },
    SET_USERNAME (state, val) {
      state.userName = val
    }
  },
  actions: {
  },
  modules: {
  }
})
