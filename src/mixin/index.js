import { Toast } from 'vant'

export default {
  data () {
    return {
    }
  },
  methods: {
    /*
    加载
    */
    toast (type) {
      Toast.loading({
        message: type,
        forbidClick: true
      })
    },
    clearToast () {
      Toast.clear()
    }
  }
}
