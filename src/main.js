import '@/utils/console'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import 'amfe-flexible' // 移动端适配
import '@/VueComponent'
axios({ url: '/api/nc/article/headline/T1348647853363/0-40.html' }).then(res => console.log(res, 'res'))
console.log(process.env)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
