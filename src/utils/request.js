// 基于axios封装网络请求
import router from '@/router/index'
import theAxios from 'axios'
import { getToken, setToken, removeToken } from '@/utils/token'
import { Toast } from 'vant'
import Notify from '@/ui/Notify'
import { getNewuserTokenAPI } from '@/api/index'
// 1.0创建axios
const axios = theAxios.create({
  // 1.1 基地址baseURL
  // baseURL: 'http://123.57.109.30:8000',
  baseURL: 'http://toutiao.itheima.net',
  // baseURL: 'http://geek.itheima.net',
  // 1.2：20秒无响应
  timeout: 20000
})
// 2.0添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 2.1 在发起请求前做什么
  // 2.2统一携带token,没有叫undefined，null被赋值才叫空
  if (getToken()?.length > 0 && config.headers.Authorization === undefined) {
    config.headers.Authorization = `Bearer ${getToken()}`
  }
  return config
}, function (err) {
  // 2.2 对请求错误做些什么
  return Promise.reject(err)
})
// 3.0 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 当状态码为2xx/3xx开头的进这里
  // 对响应数据做点什么
  return response
}, async function (error) {
  // 响应状态码4xx/5xx进这里
  // 对响应错误做点什么
  console.dir(error)
  if (error.response.status === 401) { // 身份过期
    // console.log(router.currentRoute.fullPath, 'router') // 获取哪个页面token失效的路径，用来做操作未遂的跳转页面
    // token验证
    // 方式1: 去登录页重新登录, token无用, 清掉 - 确保路由守卫if进不去(对用户不友好)
    // 不可以使用this.$router,这里的this不是vue实例
    // Notify({ type: 'warning', message: '身份过期' })
    removeToken()
    // router.replace('/Login')
    // 方式2： 用  refresh_token  更换  tokrn  (无感知的刷新token,不用返回登录页请求新的token，直接在相应拦截器里面根据状态码进行重新请求即可)
    // 2.1  当用户身份过期需要更新本地的token
    // 2.2  获取新的token
    // 2.3  把新的token替换原来的token,存在本地
    // 2.4  更新新的token在请求头里面，未完成报错的请求再发一遍===>error.config就是上一次请求未成功的请求
    const res = await getNewuserTokenAPI()
    setToken(res.data.data.token || '')
    error.config.headers.Authorization = `Bearer ${res.data.data.token}`
    return axios(error.config)
    // 2.5  接下来判断如果refresh_token也失效了的状况下，就让用户跳到登录页面重新登录
  } else if (error.response.status === 500 && error.response.config.url === '/v1_0/authorizations' && error.response.config.method === 'put') {
    console.log('refresh_token也失效了')
    // 2.6  清楚所有的token，跳转的登录页
    Notify({ type: 'warning', message: '身份过期' })
    localStorage.clear()
    // router.currentRoute.fullPath  //登录未遂的页面
    router.replace(`/Login?path=${router.currentRoute.fullPath}`)
  } else if (error.response.status === 404) {
    Toast.fail(error.response.statusText)
    setTimeout(() => {
      router.push('/Layout')
    }, 700)
  }

  return Promise.reject(error)
})
export default ({ url, method = 'GET', params = {}, data = {}, headers = {} }) => {
  return axios({
    url,
    method,
    params,
    data,
    headers
  })
}
