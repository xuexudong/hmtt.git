// 对token进行存取
const key = 'geek-itheima'
// 存
export const setToken = (token) => {
  localStorage.setItem(key, token)
}
// 取
export const getToken = () => {
  return localStorage.getItem(key)
}
// 删除
export const removeToken = () => {
  return localStorage.removeItem(key)
}
