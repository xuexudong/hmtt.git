console.log(process.env)
if (process.env.NODE_ENV === 'production') {
  console.log = function () { }
  console.error = function () { }
  console.dir = function () { }
  console.warning = function () { }
}
