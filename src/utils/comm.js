
// 防抖
let timer = null
export const constinpputFn = (txt, callback) => {
  clearTimeout(timer)
  timer = setTimeout(() => {
    console.log(txt)
    callback()
  }, 500)
}
// 处理字符串高亮
export const lightFn = (str, target) => {
// str 字符串;后台返回的
// target 关键字，自己输入的字
  if (!str) return // 没有内容停止
  const reg = new RegExp(target, 'ig') // 匹配正则
  return str.replace(reg, (match) => { // match：关键字匹配的值(尽量保持原样
    return `<span style="color: red">${match}</span>`
  })
}
