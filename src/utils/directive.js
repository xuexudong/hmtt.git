// 创建全局自定义指令
// 写法1
// Vue.directive('focus', {
//   inserted (el) {
//     // vant是一个div，所以el.focus对其不作用，用js找el下面的input
//     const input = el.querySelector('input')
//     input.focus()
//   }
// })
// 写法2
const directiveObg = {
  install (Vue) {
    Vue.directive('focus', {
      inserted (el) {
        console.log(el.nodeName, '’el.nodeName')
        // el.nodeName  当前实例的名字 （input）
        if (el.nodeName === 'INPUT' || el.nodeName === 'TEXTAREA') {
          // 原生的直接绑定时间
          el.focus()
        } else {
          // vant是一个div，所以el.focus对其不作用，用js找el下面的input
          const INPUT = el.querySelector('input')
          const TEXTAREA = el.querySelector('textarea')
          if (INPUT || TEXTAREA) {
            INPUT && INPUT.focus()
            TEXTAREA && TEXTAREA.focus()
          } else {
            console.log('不好意思小主~~都找不到标签')
          }
        }
      },
      // 组件更新的时候调用
      updated (el) {
        // el.nodeName  当前实例的名字 （input）
        if (el.nodeName === 'input' || el.nodeName === 'TEXTAREA') {
          // 原生的直接绑定时间
          el.focus()
        } else {
          // vant是一个div，所以el.focus对其不作用，用js找el下面的input
          const INPUT = el.querySelector('input')
          const TEXTAREA = el.querySelector('textarea')
          if (INPUT || TEXTAREA) {
            INPUT && INPUT.focus()
            TEXTAREA && TEXTAREA.focus()
          } else {
            console.log('不好意思小主~~都找不到标签')
          }
        }
      }
    })
  }
}
export default directiveObg
