import request from '../utils/request.js'
// 首页登录请求
export const loaginAPI = ({
  mobile,
  code
}) => request({
  url: '/v1_0/authorizations',
  method: 'POST',
  data: {
    mobile,
    code
  }
})
// 获取用户的频道
export const userChannelAPI = () => request({
  url: '/v1_0/user/channels'
})
// 重置客户频道 //删除频道
export const updateChannelsApi = ({ channels }) => request(
  {
    url: '/v1_0/user/channels',
    method: 'PUT',
    data: {
      channels
    }
  }
)
// 用户资料
export const userInfoAPI = () => {
  return request({
    url: '/v1_0/user'
  })
}
// 获取用户生日
export const userProfileAPI = () => {
  return request({
    url: '/v1_0/user/profile'
  })
}

// 修改用户头像
// 用户- 更新头像
// 注意: formObj的值必须是一个表单对象
// '{"a": 10, "b": 20}' // 对象格式的JSON字符串
// new FormData() // 表单对象
export const updatePhotoAPI = (formObj) => {
  return request({
    url: '/v1_0/user/photo',
    method: 'PATCH',
    data: formObj
    // 如果你的请求体内容是表单对象, 浏览器会自动携带请求头Content-Type为multipart/form-data
  })
}
// 更新用户名字
export const updateProfileAPI = ({ birthday, userName }) => {
  return request({
    url: '/v1_0/user/profile',
    method: 'PATCH',
    data: {
      birthday: birthday,
      name: userName
    }
  })
}
// 如果用户的token过期的话，更新用户的token
export const getNewuserTokenAPI = () => {
  return request({
    url: '/v1_0/authorizations',
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('refresh_token')}`
    }

  })
}
