// import request from '../utils/request.js'
//  export *    写法：即引入又向外抛出所有的articleDetail里面的方法
export * from '@/api/articleDetail' // 文章相关接口
export * from '@/api/userApi' // 用户相关接口
export * from '@/api/searchApi'
