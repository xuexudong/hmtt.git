import request from '../utils/request.js'
// 搜索，联想词相关接口
// 联想建议  suggestListAPI
export const suggestListAPI = ({ keyWord }) => request({
  url: '/v1_0/suggestion',
  params: {
    q: keyWord
  }
})
// 搜索结果页面
export const searchSure = ({ q, page }) => request({
  url: '/v1_0/search',
  params: {
    q,
    page
  }
})
