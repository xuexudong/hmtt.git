// 文章相关接口
import request from '../utils/request.js'
// 获取推荐文章
export const articlesListAPI = ({ channelId, timestamp }) => request(
  {
    url: '/v1_0/articles',
    params: {
      channel_id: channelId,
      timestamp
    }
  }
)
// 反馈文章
export const dislikeArticleAPI = ({ target }) => request(
  {
    url: '/v1_0/article/dislikes',
    method: 'POST',
    data: {
      target
    }
  }
)
// 举报文章
export const reportArticleAPI = ({ target, type }) => request(
  {
    url: '/v1_0/article/reports',
    method: 'POST',
    data: {
      target, type, remark: '举报理由'
    }
  }
)
// 文章详情
export const detailAPI = ({ id }) => request({
  // :id是后台规定的参数名
  // 前端要在对应路径位置传值(不要写:)
  url: `/v1_0/articles/${id}`
})
// 关注作者
export const followedUserAPI = ({ target }) => request({
  url: '/v1_0/user/followings',
  method: 'POST',
  data: {
    target
  }
})
// 取关作者
export const unfollowedUserAPI = ({ uid }) => request({
  url: `/v1_0/user/followings/${uid}`,
  method: 'DELETE'
})
// 取消点赞
export const articleDisLikeAPI = ({ artId }) => request({
  url: `/v1_0/article/likings/${artId}`,
  method: 'DELETE'
})
// 点赞
export const articleLikeAPI = ({ target }) => request({
  url: '/v1_0/article/likings',
  method: 'POST',
  data: {
    target
  }
})
// 获取评论
export const commentListAPI = ({ artId, offset, limit = 10 }) => request({
  url: '/v1_0/comments',
  method: 'GET',
  params: {
    type: 'a',
    source: artId,
    offset,
    limit
  }
})
// 评论 - 喜欢
export const commentLikingAPI = ({ comId }) => {
  return request({
    url: '/v1_0/comment/likings',
    method: 'POST',
    data: {
      target: comId
    }
  })
}
// 评论-取消喜欢
export const commentDisLikingAPI = ({ comId }) => {
  return request({
    url: `/v1_0/comment/likings/${comId}`,
    method: 'DELETE'
  })
}
// 发布评论
export const sendCommentAPI = ({ Id, content, artid = null }) => {
  const obj = {
    target: Id,
    content: content
  }
  if (artid !== null) {
    obj.artid = artid
  }
  return request({
    url: '/v1_0/comments',
    method: 'POST',
    data: obj
  })
}
// 获取所有频道
export const getAllChannelsAPI = () => request({
  url: '/v1_0/channels'
})
