import Vue from 'vue'
import directiveObg from '@/utils/directive' // 自定义指令
import { NavBar, Button, Form, Field, Notify, Loading, Toast, Tabbar, TabbarItem, Icon, Tab, Tabs, Cell, CellGroup, List, PullRefresh, Popup, ActionSheet, Row, Col, Badge, Search, Image as VanImage, Divider, Tag, Dialog, Uploader, DatetimePicker, Lazyload } from 'vant'
Vue.config.productionTip = false//阻止启动生产消息

Vue.use(directiveObg).use(NavBar).use(Button).use(Form).use(Field).use(Notify).use(Loading).use(Toast).use(Tabbar).use(TabbarItem).use(Icon).use(Tab).use(Tabs).use(Cell).use(CellGroup).use(List).use(PullRefresh).use(Popup).use(ActionSheet).use(Row).use(Col).use(Badge).use(Search).use(VanImage).use(Divider).use(Tag).use(CellGroup).use(Dialog).use(Uploader).use(DatetimePicker).use(Lazyload, {
  error: 'https://tse4-mm.cn.bing.net/th/id/OIP-C.FuWXD5OAEc4mJwGp4swt0QHaEF?w=305&h=180&c=7&r=0&o=5&dpr=1.25&pid=1.7' // 错误图片提示
})
