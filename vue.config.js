const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 代理请求
  devServer: {
    proxy: {
      '/api': {
        target: 'http://c.m.163.com',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    },
  },
  lintOnSave: false,
  publicPath: './'// 告诉webpack打包的index.html以./开头
})
